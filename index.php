<?php
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);

    require_once "autoload.php";
?>

<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="assets/style.css">
    <title>The Secret Game</title>
</head>
<body>
    <?php include "header.php"; ?>
    
    <div id="main_content" class="flex">
    <?php
        $WORD = "Ceci est un test"; 
        $game = new SecretWordGame($WORD);
        $arr = $game->try(null);

        
        if (!isset($_POST) || empty($_POST) || !isset($_POST["letter"])){
            $game->generateInput($arr);
            $arr = $game->try(null);
        } else {
            $arr = $_POST["letter"];
            $arr = $game->try($arr);
            $game->generateInput($arr);
        }
    
    ?>

    </div>

    <?php include "footer.php"; ?>
    
    <script src="script.js"></script>
</body>
</html>