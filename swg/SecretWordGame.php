<?php
    // namespace swg;

    class SecretWordGame{

        private ?array $word_array = null;

        public function getWordArray() : array { return $this->word_array; }

        public function __construct(string $secret){
            $this->word_array = str_split(strtolower($secret));
        }

        public static function strToArray(string $word){ 
            return str_split(strtolower($word));
        }
        public static function arrayToStr(?array $arr){
            if (isset($arr) && !empty($arr)) {
                return join("", $arr);
            }
            else return '';
        }

        public function try(?array $word=null) : array{
            $result = "";
            $win = ($word === $this->word_array) ? true : false;

            for ($i = 0; $i < count($this->word_array); $i++) {
                if (isset($word[$i])){
                    if ($this->word_array[$i] == $word[$i]) {
                        $result .= $word[$i];
                    } else if ($this->word_array[$i] == " ") $result .= " ";
                    else $result .= "?";
                } else {
                    if ($this->word_array[$i] == " ") $result .= " ";
                    else $result .= "?";
                }
            }
            // echo var_dump($word);
            return array("word"=>$word, "win"=>$win, "result"=>$result);
        }

        public function generateInput(?array $response) : void {
            ?>
            <form id="main_form" class="flex" method="post">
                <div class="input-container">
                    <?php 
                        foreach(self::strToArray($response["result"]) as $key => $char) {
                            if ($char == "?") {?>
                                <input type='text' class="input-item" name="letter[<?php echo $key?>]">
                            <?php
                            } else if ($char == " ") { ?>
                                <input type='hidden' class="input-item" name="letter[<?php echo $key?>]" value=" ">
                                <div class="item-separator"></div>
                            <?php
                            }
                            else {?>
                                <input type='text' class="input-item" name="letter[<?php echo $key?>]" value=<?php echo $char ?> disabled>
                                <input type='hidden' class="input-item" name="letter[<?php echo $key?>]" value=<?php echo $char ?>>
                            <?php
                            }
                        }
                    ?>
                </div>
                <button id="try_btn" type="submit" name="try">Try</button>
            </form>
            <?php
        }


    }
?>