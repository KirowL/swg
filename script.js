document.addEventListener("DOMContentLoaded", () => {
    let form = document.getElementById("main_form");
    let inputsBase = document.getElementsByClassName("input-item");

    let inputs = [];
    for (let i = 0; i < inputsBase.length; i++) {
        if (inputsBase[i].value != " ") inputs.push(inputsBase[i]);
    }

    for (let i = 0; i < inputsBase.length; i++) {
        if (inputsBase[i].type != "hidden" && !inputsBase[i].disabled) {
            inputsBase[i].focus();
            break;
        }
    }

    for (let i = 0; i < inputs.length; i++) {
        inputs[i].addEventListener("input", () => {
            let val = inputs[i].value;
            if (val.length >= 1) {
                inputs[i].value = val.slice(0, 1);
                if (i + 1 < inputs.length) {
                    inputs[i + 1].focus();
                } else {
                    if (val.length >= 1) {
                        inputs[i].value = val[1];
                    }
                }
            }
        })
    }
})